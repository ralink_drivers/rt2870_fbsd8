
SRCS =	device_if.h \
		bus_if.h \
		usbdevs.h \
		opt_usb.h \
		rt2870_io.c \
		rt2870_read_eeprom.c \
		rt2870_led.c \
		rt2870_rf.c \
		rt2870_amrr.c \
		rt2870.c

KMOD = rt2870

DEBUG_FLAGS = -g
WERROR =
CFLAGS += -DRT2870_DEBUG

.include <bsd.kmod.mk>
