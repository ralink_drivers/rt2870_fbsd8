
/*-
 * Copyright (c) 2009-2010 Alexander Egorenkov <egorenar@gmail.com>
 * Copyright (c) 2009 Damien Bergamini <damien.bergamini@free.fr>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "rt2870_io.h"
#include "rt2870_reg.h"

/*
 * Defines and macros
 */

#define RT2870_IO_USB_REQ_MCU_CNTL					0x1
#define RT2870_IO_USB_REQ_MAC_WRITE_MULTI			0x2
#define RT2870_IO_USB_REQ_MAC_READ_MULTI			0x7
#define RT2870_IO_USB_REQ_EEPROM_READ_MULTI			0x9

#define RT2870_IO_USB_VALUE_MCU_RESET				0x1
#define RT2870_IO_USB_VALUE_MCU_RUN					0x8

#define RT2870_IO_BYTE_CRC16(byte, crc)				\
	((uint16_t) (((crc) << 8) ^ rt2870_io_ccitt16[(((crc) >> 8) ^ (byte)) & 255]))

/*
 * Static function prototypes
 */

static int rt2870_io_vendor_req(struct rt2870_softc *sc,
	uint8_t reqtype, uint8_t req, uint16_t value, uint16_t index,
	void *buf, uint16_t len);

static uint8_t rt2870_io_byte_rev(uint8_t byte);

/*
 * Static variables
 */

static const uint16_t rt2870_io_ccitt16[] =
{
	0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50a5, 0x60c6, 0x70e7,
	0x8108, 0x9129, 0xa14a, 0xb16b, 0xc18c, 0xd1ad, 0xe1ce, 0xf1ef,
	0x1231, 0x0210, 0x3273, 0x2252, 0x52b5, 0x4294, 0x72f7, 0x62d6,
	0x9339, 0x8318, 0xb37b, 0xa35a, 0xd3bd, 0xc39c, 0xf3ff, 0xe3de,
	0x2462, 0x3443, 0x0420, 0x1401, 0x64e6, 0x74c7, 0x44a4, 0x5485,
	0xa56a, 0xb54b, 0x8528, 0x9509, 0xe5ee, 0xf5cf, 0xc5ac, 0xd58d,
	0x3653, 0x2672, 0x1611, 0x0630, 0x76d7, 0x66f6, 0x5695, 0x46b4,
	0xb75b, 0xa77a, 0x9719, 0x8738, 0xf7df, 0xe7fe, 0xd79d, 0xc7bc,
	0x48c4, 0x58e5, 0x6886, 0x78a7, 0x0840, 0x1861, 0x2802, 0x3823,
	0xc9cc, 0xd9ed, 0xe98e, 0xf9af, 0x8948, 0x9969, 0xa90a, 0xb92b,
	0x5af5, 0x4ad4, 0x7ab7, 0x6a96, 0x1a71, 0x0a50, 0x3a33, 0x2a12,
	0xdbfd, 0xcbdc, 0xfbbf, 0xeb9e, 0x9b79, 0x8b58, 0xbb3b, 0xab1a,
	0x6ca6, 0x7c87, 0x4ce4, 0x5cc5, 0x2c22, 0x3c03, 0x0c60, 0x1c41,
	0xedae, 0xfd8f, 0xcdec, 0xddcd, 0xad2a, 0xbd0b, 0x8d68, 0x9d49,
	0x7e97, 0x6eb6, 0x5ed5, 0x4ef4, 0x3e13, 0x2e32, 0x1e51, 0x0e70,
	0xff9f, 0xefbe, 0xdfdd, 0xcffc, 0xbf1b, 0xaf3a, 0x9f59, 0x8f78,
	0x9188, 0x81a9, 0xb1ca, 0xa1eb, 0xd10c, 0xc12d, 0xf14e, 0xe16f,
	0x1080, 0x00a1, 0x30c2, 0x20e3, 0x5004, 0x4025, 0x7046, 0x6067,
	0x83b9, 0x9398, 0xa3fb, 0xb3da, 0xc33d, 0xd31c, 0xe37f, 0xf35e,
	0x02b1, 0x1290, 0x22f3, 0x32d2, 0x4235, 0x5214, 0x6277, 0x7256,
	0xb5ea, 0xa5cb, 0x95a8, 0x8589, 0xf56e, 0xe54f, 0xd52c, 0xc50d,
	0x34e2, 0x24c3, 0x14a0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
	0xa7db, 0xb7fa, 0x8799, 0x97b8, 0xe75f, 0xf77e, 0xc71d, 0xd73c,
	0x26d3, 0x36f2, 0x0691, 0x16b0, 0x6657, 0x7676, 0x4615, 0x5634,
	0xd94c, 0xc96d, 0xf90e, 0xe92f, 0x99c8, 0x89e9, 0xb98a, 0xa9ab,
	0x5844, 0x4865, 0x7806, 0x6827, 0x18c0, 0x08e1, 0x3882, 0x28a3,
	0xcb7d, 0xdb5c, 0xeb3f, 0xfb1e, 0x8bf9, 0x9bd8, 0xabbb, 0xbb9a,
	0x4a75, 0x5a54, 0x6a37, 0x7a16, 0x0af1, 0x1ad0, 0x2ab3, 0x3a92,
	0xfd2e, 0xed0f, 0xdd6c, 0xcd4d, 0xbdaa, 0xad8b, 0x9de8, 0x8dc9,
	0x7c26, 0x6c07, 0x5c64, 0x4c45, 0x3ca2, 0x2c83, 0x1ce0, 0x0cc1,
	0xef1f, 0xff3e, 0xcf5d, 0xdf7c, 0xaf9b, 0xbfba, 0x8fd9, 0x9ff8,
	0x6e17, 0x7e36, 0x4e55, 0x5e74, 0x2e93, 0x3eb2, 0x0ed1, 0x1ef0
};

/*
 * rt2870_io_mac_read
 */
uint32_t rt2870_io_mac_read(struct rt2870_softc *sc, uint16_t reg)
{
	uint32_t val;

	rt2870_io_mac_read_multi(sc, reg, &val, sizeof(val));

	return le32toh(val);
}

/*
 * rt2870_io_mac_read_multi
 */
void rt2870_io_mac_read_multi(struct rt2870_softc *sc,
	uint16_t reg, void *buf, size_t len)
{
	int error;

	error = rt2870_io_vendor_req(sc, UT_READ_VENDOR_DEVICE,
		RT2870_IO_USB_REQ_MAC_READ_MULTI, 0, reg, buf, len);
	if (error != 0)
		printf("%s: could not multi read MAC register: reg=0x%04x, error=%s\n",
			device_get_nameunit(sc->dev), reg, usbd_errstr(error));
}

/*
 * rt2870_io_mac_write
 */
void rt2870_io_mac_write(struct rt2870_softc *sc,
	uint16_t reg, uint32_t val)
{
	uint32_t tmp;

	tmp = htole32(val);

	rt2870_io_mac_write_multi(sc, reg, &tmp, sizeof(uint32_t));
}

/*
 * rt2870_io_mac_write_multi
 */
void rt2870_io_mac_write_multi(struct rt2870_softc *sc,
	uint16_t reg, const void *buf, size_t len)
{
	const uint16_t *ptr;
	int i, error;

	len += len % sizeof(uint16_t);
	ptr = buf;

	i = 0;

	do
	{
		error = rt2870_io_vendor_req(sc, UT_WRITE_VENDOR_DEVICE,
			RT2870_IO_USB_REQ_MAC_WRITE_MULTI, *ptr++, reg + i, NULL, 0);

		i += sizeof(uint16_t);
		len -= sizeof(uint16_t);
	} while (len > 0 && error == 0);

	if (error != 0)
		printf("%s: could not multi write MAC register: reg=0x%04x, error=%s\n",
			device_get_nameunit(sc->dev), reg, usbd_errstr(error));
}

/*
 * rt2870_io_mac_set_region_4
 */
void rt2870_io_mac_set_region_4(struct rt2870_softc *sc,
	uint16_t reg, uint32_t val, size_t len)
{
	int i;

	for (i = 0; i < len; i += sizeof(uint32_t))
		rt2870_io_mac_write(sc, reg + i, val);
}

/*
 * rt2870_io_eeprom_read
 */
uint16_t rt2870_io_eeprom_read(struct rt2870_softc *sc, uint16_t addr)
{
	uint16_t val;

	rt2870_io_eeprom_read_multi(sc, addr, &val, sizeof(val));

	return le16toh(val);
}

/*
 * rt2870_io_eeprom_read_multi
 */
void rt2870_io_eeprom_read_multi(struct rt2870_softc *sc,
	uint16_t addr, void *buf, size_t len)
{
	int error;

	error = rt2870_io_vendor_req(sc, UT_READ_VENDOR_DEVICE,
		RT2870_IO_USB_REQ_EEPROM_READ_MULTI, 0, addr, buf, len);
	if (error != 0)
		printf("%s: could not multi read EEPROM: addr=0x%04x, error=%s\n",
			device_get_nameunit(sc->dev), addr, usbd_errstr(error));
}

/*
 * rt2870_io_bbp_read
 */
uint8_t rt2870_io_bbp_read(struct rt2870_softc *sc, uint8_t reg)
{
	uint32_t val;
	int ntries;

	for (ntries = 0; ntries < 10; ntries++)
		if (!(rt2870_io_mac_read(sc, RT2870_REG_BBP_CSR_CFG) &
			RT2870_REG_BBP_CSR_BUSY))
			break;

	if (ntries == 10)
	{
		printf("%s: could not read BBP: reg=0x%02x\n",
			device_get_nameunit(sc->dev), reg);
		return 0;
	}

	val = RT2870_REG_BBP_CSR_BUSY |
		RT2870_REG_BBP_CSR_READ |
		((reg & RT2870_REG_BBP_REG_MASK) << RT2870_REG_BBP_REG_SHIFT);

	rt2870_io_mac_write(sc, RT2870_REG_BBP_CSR_CFG, val);

	for (ntries = 0; ntries < 10; ntries++)
	{
		val = rt2870_io_mac_read(sc, RT2870_REG_BBP_CSR_CFG);
		if (!(val & RT2870_REG_BBP_CSR_BUSY))
			return ((val >> RT2870_REG_BBP_VAL_SHIFT) &
				RT2870_REG_BBP_VAL_MASK);

		DELAY(1);
	}

	printf("%s: could not read BBP: reg=0x%02x\n",
		device_get_nameunit(sc->dev), reg);

	return 0;
}

/*
 * rt2870_io_bbp_write
 */
void rt2870_io_bbp_write(struct rt2870_softc *sc, uint8_t reg, uint8_t val)
{
	uint32_t tmp;
	int ntries;

	for (ntries = 0; ntries < 10; ntries++)
		if (!(rt2870_io_mac_read(sc, RT2870_REG_BBP_CSR_CFG) &
			RT2870_REG_BBP_CSR_BUSY))
			break;

	if (ntries == 10)
	{
		printf("%s: could not write to BBP: reg=0x%02x\n",
			device_get_nameunit(sc->dev), reg);
		return;
	}

	tmp = RT2870_REG_BBP_CSR_BUSY |
		((reg & RT2870_REG_BBP_REG_MASK) << RT2870_REG_BBP_REG_SHIFT) |
		((val & RT2870_REG_BBP_VAL_MASK) << RT2870_REG_BBP_VAL_SHIFT);

	rt2870_io_mac_write(sc, RT2870_REG_BBP_CSR_CFG, tmp);
}

/*
 * rt2870_io_rf_write
 */
void rt2870_io_rf_write(struct rt2870_softc *sc, uint8_t reg, uint32_t val)
{
	int ntries;

	for (ntries = 0; ntries < 10; ntries++)
		if (!(rt2870_io_mac_read(sc, RT2870_REG_RF_CSR_CFG0) &
			RT2870_REG_RF_BUSY))
			break;

	if (ntries == 10)
	{
		printf("%s: could not write to RF: reg=0x%02x\n",
			device_get_nameunit(sc->dev), reg);
		return;
	}

	rt2870_io_mac_write(sc, RT2870_REG_RF_CSR_CFG0, val);
}

/*
 * rt2870_io_mcu_cmd
 */
void rt2870_io_mcu_cmd(struct rt2870_softc *sc, uint8_t cmd,
	uint8_t token, uint16_t arg)
{
	uint32_t tmp;
	int ntries;

	for (ntries = 0; ntries < 100; ntries++)
	{
		if (!(rt2870_io_mac_read(sc, RT2870_REG_H2M_MAILBOX) &
			RT2870_REG_H2M_BUSY))
			break;

		DELAY(2);
	}

	if (ntries == 100)
	{
		printf("%s: could not read H2M: cmd=0x%02x\n",
			device_get_nameunit(sc->dev), cmd);
		return;
	}

	tmp = RT2870_REG_H2M_BUSY | (token << 16) | arg;

	rt2870_io_mac_write(sc, RT2870_REG_H2M_MAILBOX, tmp);
	rt2870_io_mac_write(sc, RT2870_REG_H2M_HOST_CMD, cmd);
}

/*
 * rt2870_io_mcu_load_ucode
 */
int rt2870_io_mcu_load_ucode(struct rt2870_softc *sc,
	const uint8_t *ucode, size_t len)
{
	int i, error, ntries;
	uint16_t crc;

	for (i = 0, crc = 0xffff; i < len - 2; i++)
		crc = RT2870_IO_BYTE_CRC16(rt2870_io_byte_rev(ucode[i]), crc);

	if (ucode[len - 2] != rt2870_io_byte_rev(crc >> 8) ||
		ucode[len - 1] != rt2870_io_byte_rev(crc))
	{
		printf("%s: wrong microcode crc\n",
			device_get_nameunit(sc->dev));
		return EINVAL;
	}

	rt2870_io_mac_write_multi(sc, RT2870_REG_MCU_UCODE_BASE, ucode, len);

	rt2870_io_mac_write(sc, RT2870_REG_H2M_MAILBOX_CID, 0xffffffff);
	rt2870_io_mac_write(sc, RT2870_REG_H2M_MAILBOX_STATUS, 0xffffffff);

	error = rt2870_io_vendor_req(sc, UT_WRITE_VENDOR_DEVICE,
		RT2870_IO_USB_REQ_MCU_CNTL, RT2870_IO_USB_VALUE_MCU_RUN, 0, NULL, 0);
	if (error != 0)
	{
		printf("%s: could not run firmware: error=%s\n",
			device_get_nameunit(sc->dev), usbd_errstr(error));
		return error;
	}

	for (ntries = 0; ntries < 1000; ntries++)
	{
		if (rt2870_io_mac_read(sc, RT2870_REG_PBF_SYS_CTRL) &
			RT2870_REG_MCU_READY)
			break;

		DELAY(1000);
	}

	if (ntries == 1000)
	{
		printf("%s: timeout waiting for MCU to initialize\n",
			device_get_nameunit(sc->dev));
		return ETIMEDOUT;
	}

	return 0;
}

/*
 * rt2870_io_mcu_reset
 */
int rt2870_io_mcu_reset(struct rt2870_softc *sc)
{
	return rt2870_io_vendor_req(sc, UT_WRITE_VENDOR_DEVICE,
		RT2870_IO_USB_REQ_MCU_CNTL, RT2870_IO_USB_VALUE_MCU_RESET, 0, NULL, 0);
}

/*
 * rt2870_io_vendor_req
 */
static int rt2870_io_vendor_req(struct rt2870_softc *sc,
	uint8_t reqtype, uint8_t req, uint16_t value, uint16_t index,
	void *buf, uint16_t len)
{
	struct usb_device_request usb_req;
	usb_error_t error;
	int ntries;

	for (ntries = 0; ntries < 10; ntries++)
	{
		usb_req.bmRequestType = reqtype;
		usb_req.bRequest = req;
		USETW(usb_req.wValue, value);
		USETW(usb_req.wIndex, index);
		USETW(usb_req.wLength, len);

		error = usbd_do_request_flags(sc->usb_dev, &sc->lock, &usb_req, buf,
			0, NULL, 250);
		if (error == 0)
			break;

		DELAY(5000);
	}

	return error;
}

/*
 * rt2870_io_byte_rev
 */
static uint8_t rt2870_io_byte_rev(uint8_t byte)
{
	int i;
	uint8_t tmp;

	for(i = 0, tmp = 0; ; i++)
	{
		if(byte & 0x80)
			tmp |= 0x80;

		if(i == 7)
			break;

		byte <<= 1;
		tmp >>= 1;
	}

	return tmp;
}
